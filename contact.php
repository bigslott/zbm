<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="./stylesheets/styles.css">
<link href="//vjs.zencdn.net/4.5/video-js.css" rel="stylesheet">
<title>zenbu Social</title>
<style type="text/css">
  * {max-width: 100%} /* the usual RWD shebang */

  .video-js {
      width: auto !important; /* override the plugin's inline dims to let vids scale fluidly */
      height: auto !important;
  }

  .video-js video {position: relative !important;}
   /* The video should expand to force the height of the containing div.
   One in-flow element is good. As long as everything else in the container
   div stays `position: absolute` we're okay */

  .video-js .vjs-big-play-button {
      top: 50%; /* errrr, why isn't the play button centered in the default skin? :) */
      left: 50%;
      margin:-4em 0 0 -6em; /* the old negative margin trick */
  }

  .videocontent {
    max-height: 418px;
    max-width: 720px;
    margin: 0 auto;
  }
</style>
</head>

<body>

<div class="navbar navbar-default navbar-fixed-top header-wrapper">
  <div class="container header">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="logo">
        <a href="/" class="navbar-brand">
          <img src="./images/logo.png" alt="zenbusocial logo"/>
        </a>
      </div>
    </div>

    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/">Home</a></li>
        <li><a href="/features.php">Features</a></li>
        <li><a href="/clients.php">Clients</a></li>
        <li><a href="/contact.php" class="active">Contact</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <div class="main-deck row contact-main">
    <div class="col-lg-4">
      <h2 class="danger">Contact info</h2>
        <div class="contact-left">Address:</div>
        <div class="contact-right">Århusgade 129. 1.</br>2150 Nordhavn,</br>Denmark</div>
        <div class="contact-left">Mobile:</div>
        <div class="contact-right">+45 00 00 00</div>
        <div class="contact-left">Phone:</div>
        <div class="contact-right">+45 00 00 00</div>
        <div class="contact-left">Email:</div>
        <div class="contact-right"><a href="mailto:info@zenbusocial.com">info@zenbusocial.com</a></div>
        <div class="contact-left">Website:</div>
        <div class="contact-right"><a href="http://zenbusocial.com">http://zenbusocial.com</a></div>
    </div>
    <div class="col-lg-8">
      <h2 class="danger">Get intouch</h2>
      <div class="section noprint" id="contact">

  <?php

  $name = $email = $subject = $message = '';

  if(isset($_POST['submit'])) {

    // PHP form validation
    $errormessage = '';

    $name = trim($_POST['name']);
    if($name == '') {
      $hasError = true;
      $errormessage .= '<p>- The <strong>name</strong> field is empty</p>';
    }

    $email = trim($_POST['email']);
    if($email == '') {
      $hasError = true;
      $errormessage .= '<p>- The <strong>email</strong> field is empty</p>';
    }else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", $email)){
      $hasError = true;
      $errormessage .= '<p>- The <strong>email</strong> is not a valid email</p>';
    }

    $subject = trim($_POST['subject']);
    if($subject == '') {
      $hasError = true;
      $errormessage .= '<p>- The <strong>subject</strong> field is empty</p>';
    }

    if(function_exists('stripslashes')) {
      $message = stripslashes(trim($_POST['message']));
    } else {
      $message = trim($_POST['message']);
    }
    if($message == '') {
      $hasError = true;
      $errormessage .= '<p>- The <strong>message</strong> field is empty</p>';
    }

    // PHP email
    //$emailTo = 'info@zenbusocial.com';
    $emailTo = 'bigslott@gmail.com';
    $websitename = "Zenbu Social Markting";

    if(!isset($hasError)) {

      $body = "Name: $name \n\nEmail: $email \n\nSubject: $subject \n\nMessage:\n $message";
      $headers = 'From: '.$websitename.' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

      mail($emailTo, $subject, $body, $headers);
      $emailSent = true;
    }
  }else{ ?>

   <!--  <div class="alert alert-info">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div> -->

  <?php } ?>

  <?php
  // if errors
  if(isset($hasError)) { ?>
    <div class="alert alert-error">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4>Please correct the following error(s):</h4><br/>
      <?php echo $errormessage; ?>
    </div>
  <?php } ?>

  <?php
  // if email is sent
  if(isset($emailSent) && $emailSent == true) { ?>
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4>Email Successfully Sent!</h4><br/>
      <p>Thank you for using our contact form <strong><?php echo $name;?></strong></p>
      <p>Your email was successfully sent.</p>
    </div>
    <?php
    $name = $email = $subject = $message = ''; //reset values to clear fields
  } ?>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>#contact" class="form-horizontal zenbu-contact">
    <fieldset>


      <div class="control-group">
        <!-- <label class="control-label" for="form-name">Your name</label> -->
        <div class="controls">
          <div class="input-append">
            <input type="text" placeholder="Your name" id="form-name" name="name" class="input-xlarge" value="<?php echo $name; ?>">
            <!-- <span class="add-on"><span class="glyphicons user" ><i></i></span></span> -->
          </div>
        </div>
      </div>

      <div class="control-group">
        <!-- <label class="control-label" for="form-email">Yout e-mail address</label> -->
        <div class="controls">
          <div class="input-append">
            <input type="text" placeholder="Your email" id="form-email" name="email" class="input-xlarge" value="<?php echo $email; ?>">
            <!-- <span class="add-on">@</span> -->
          </div>
        </div>
      </div>

      <div class="control-group">
        <!-- <label class="control-label" for="form-subject">Subject</label> -->
        <div class="controls">
          <div class="input-append">
            <input type="text" placeholder="Email Subject" id="form-subject" name="subject" class="input-xlarge" value="<?php echo $subject; ?>">
            <!-- <span class="add-on"><span class="glyphicons circle_question_mark"><i></i></span></span> -->
          </div>
        </div>
      </div>

      <div class="control-group">
        <!-- <label class="control-label" for="form-message">Message</label> -->
        <div class="controls">
          <div class="input-append">
            <textarea rows="6" placeholder="Your message" id="form-message" name="message" class="input-xlarge"><?php echo $message; ?></textarea>
            <!-- <span class="add-on"><span class="glyphicons pen"><i></i></span></span> -->
          </div>
        </div>
      </div>

      <div class="control-group">
        <div class="controls">
          <button type="submit" class="btn btn-primary input-medium" name="submit">Submit</button>
        </div>
      </div>

    </fieldset>
  </form>
</div><!--/section-->

    </div>
  </div>
</div>

<div class="footer"></div>

<script src="//vjs.zencdn.net/4.5/video.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
</body>

</html>
