<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="./stylesheets/styles.css">
<link href="//vjs.zencdn.net/4.5/video-js.css" rel="stylesheet">
<title>zenbu Social</title>
<style type="text/css">
  * {max-width: 100%} /* the usual RWD shebang */

  .video-js {
      width: auto !important; /* override the plugin's inline dims to let vids scale fluidly */
      height: auto !important;
  }

  .video-js video {position: relative !important;}
   /* The video should expand to force the height of the containing div.
   One in-flow element is good. As long as everything else in the container
   div stays `position: absolute` we're okay */

  .video-js .vjs-big-play-button {
      top: 50%; /* errrr, why isn't the play button centered in the default skin? :) */
      left: 50%;
      margin:-4em 0 0 -6em; /* the old negative margin trick */
  }

  .videocontent {
    max-height: 418px;
    max-width: 720px;
    margin: 0 auto;
  }
</style>
</head>

<body>

<div class="navbar navbar-default navbar-fixed-top header-wrapper">
  <div class="container header">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="logo">
        <a href="/" class="navbar-brand">
          <img src="./images/logo.png" alt="zenbusocial logo"/>
        </a>
      </div>
    </div>

    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/">Home</a></li>
        <li><a href="/features.php" class="active">Features</a></li>
        <li><a href="/clients.php">Clients</a></li>
        <li><a href="/contact.php">Contact</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <div class="main-deck row">
    <div class="col-lg-12 features-wrapper features-wrapper--odd f1">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        <p class="feature-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
      </div>
    </div>
    <div class="col-lg-12 features-wrapper features-wrapper--even f2">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        <p class="feature-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
      </div>
    </div>
    <div class="col-lg-12 features-wrapper features-wrapper--odd f3">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        <p class="feature-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
      </div>
    </div>
    <div class="col-lg-12 features-wrapper features-wrapper--even f4">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        <p class="feature-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
      </div>
    </div>
    <div class="col-lg-12 features-wrapper features-wrapper--odd f5">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        <p class="feature-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
      </div>
    </div>
    <div class="col-lg-12 features-wrapper f6 feature-metadata--last">
      <div class="feature-metadata">
        <h2 class="feature-header">Lorem ipsum</h2>
        <p class="feature-subheadline">Track performance over time</p>
      </div>
    </div>
  </div>
</div>

<div class="footer"></div>

<script src="//vjs.zencdn.net/4.5/video.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
</body>

</html>
