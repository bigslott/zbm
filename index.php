<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="./stylesheets/styles.css">
<link href="//vjs.zencdn.net/4.5/video-js.css" rel="stylesheet">
<title>zenbu Social</title>
<style type="text/css">
  * {max-width: 100%} /* the usual RWD shebang */

  .video-js {
      width: auto !important; /* override the plugin's inline dims to let vids scale fluidly */
      height: auto !important;
  }

  .video-js video {position: relative !important;}
   /* The video should expand to force the height of the containing div.
   One in-flow element is good. As long as everything else in the container
   div stays `position: absolute` we're okay */

  .video-js .vjs-big-play-button {
      top: 50%; /* errrr, why isn't the play button centered in the default skin? :) */
      left: 50%;
      margin:-4em 0 0 -6em; /* the old negative margin trick */
  }

  .videocontent {
    max-height: 418px;
    max-width: 720px;
    margin: 0 auto;
  }
</style>
</head>

<body>

<div class="navbar navbar-default navbar-fixed-top header-wrapper">
  <div class="container header">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="logo">
        <a href="/" class="navbar-brand">
          <img src="./images/logo.png" alt="zenbusocial logo"/>
        </a>
      </div>
    </div>

    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/" class="active">Home</a></li>
        <li><a href="/features.php">Features</a></li>
        <li><a href="/clients.php">Clients</a></li>
        <li><a href="/contact.php">Contact</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <div class="main-deck row">
    <div class="video-wrapper col-lg-12">
      <div class="videocontent">
        <video id="zenbu-video" class="video-js vjs-default-skin zenbu-video" controls preload="auto" width="720" height="418" poster="./images/zenbu_intro_bg.png" data-setup='{"example_option":true}'>
          <source src="video/zenbu_intro.mp4" type='video/mp4' />
          <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
          <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
        </video>
      </div>
    </div>
  </div>
</div>

<div class="middle-deck-wrapper">
  <div class="container">
    <div class="middle-deck row">
      <div class="middle-deck--left col-lg-8 col-md-8 col-sm-12">
        <h1>Make <span class="highlighted">better</span> branded content for Facebook</h1>
      </div>
      <div class="middle-deck--right col-lg-4 col-md-4 col-sm-12">
        <a href="/" class="btn btn-danger">learn more</a>
      </div>
    </div>
  </div>

</div>

<div class="container">
  <div class="lower-deck row">
    <div class="front-page--teaser col-lg-4">
      <i class="icon icon-geometry"></i>
      <h2 class="danger">Measure</h2>
      <p>...branded content performance
  and ROI consistency</p>

    </div>
    <div class="front-page--teaser col-lg-4">
      <i class="icon icon-paper-pencil"></i>
      <h2 class="danger">Learn</h2>
      <p>... from your mistakes, share ideas
  and establish best practice.</p>
    </div>
    <div class="front-page--teaser col-lg-4">
      <i class="icon icon-board"></i>
      <h2 class="danger">Improve</h2>
      <p></p>
    </div>
  </div>
</div>

<div class="footer"></div>

<script src="//vjs.zencdn.net/4.5/video.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
</body>

</html>
